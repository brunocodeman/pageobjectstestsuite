"""
This module contains code to be executed against a Login screen
"""
from page import Page


class LoginPage(Page):
    """
    Represents a Login page.
    """

    def __init__(self, browser=None):
        super(LoginPage, self).__init__(browser)
        self.visit_url(self.config['pagedata']['url'])

    def fill_username(self):
        """
        Fills username field on login form
        """
        elem = self.get_element_in_page(
            self.config['pagedata']['username_xpath'], "xpath")
        elem.send_keys(self.metadata['pagedata']['username'])

    def fill_password(self):
        """
        Fills username field on login form
        """
        elem = self.get_element_in_page(
            self.config['pagedata']['password_xpath'], "xpath")
        elem.send_keys(self.metadata['pagedata']['password'])

    def do_login(self):
        """
        Executes the whole login action
        """
        self.fill_username()
        self.fill_password()
        self.get_element_in_page(self.config['pagedata']['login_button_name'], "name").click()


if __name__ == '__main__':
    print(__doc__)

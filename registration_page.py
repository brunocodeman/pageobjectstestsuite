import sys
from random import randint
from page import Page
from selenium.webdriver.common.keys import Keys


class RegistrationPage(Page):

    def __init__(self, browser=None):
        super(RegistrationPage, self).__init__(browser)
        self.registration_number = randint(0, sys.maxsize)
        self.visit_url(self.config['pagedata']['url'])

    def clean_divs(self):
        for div in self.config['pagedata']['divs_to_close']:
            self.remove_element(many=False, select_by="Id", element_identifier=div)

    def fill_email(self):
        email = self.metadata['pagedata'][
            'email'].format(self.registration_number)
        self.get_element_in_page(self.config['pagedata'][
            'username_xpath'], "xpath").send_keys(email)

    def click_continue(self):
        self.get_element_in_page(self.config['pagedata'][
                                 'login_button_name'], "name").click()

    def select_title(self):
        self.get_element_in_page(self.config['pagedata'][
                                 'title_id'], "id").click()

    def fill_firstname(self):
        firstname = self.metadata['pagedata'][
            'firstname'].format(self.registration_number)
        self.get_element_in_page(self.config['pagedata'][
                                 'first_name_id'], "id").send_keys(firstname)

    def fill_surname(self):
        surname = self.metadata['pagedata']['surname']
        surname_field = self.get_element_in_page(
            self.config['pagedata']['surname_id'], "id").send_keys(surname)

    def fill_birthdate(self):
        birthdate = self.metadata['pagedata']['birthdate']
        self.enable_element('xpath', self.config[
                            'pagedata']['birthdate_xpath'])
        birthdate_field = self.get_element_in_page(
            self.config['pagedata']['birthdate_xpath'],"xpath")
        birthdate_field.send_keys(Keys.DELETE)
        birthdate_field.send_keys(birthdate)

    def fill_password(self):
        password = self.metadata['pagedata']['password']
        password_field = self._browser.find_by_id(
            self.config['pagedata']['password_id']).first
        password_field.fill(password)

    def fill_password_confirmation(self):
        password_confirmation = self.metadata[
            'pagedata']['password_confirmation']
        password_confirmation_field = self._browser.find_by_id(
            self.config['pagedata']['password_confirmation_id']).first
        password_confirmation_field.fill(password_confirmation)

    def fill_postalcode(self):
        postalcode = self.metadata['pagedata']['postalcode']
        postalcode_field = self._browser.find_by_id(
            self.config['pagedata']['postalcode_id']).first
        postalcode_field.fill(postalcode)

    def fill_city(self):
        city = self.metadata['pagedata']['city']
        city_field = self._browser.find_by_id(
            self.config['pagedata']['city_id']).first
        city_field.fill(city)

    def fill_address(self):
        address = self.metadata['pagedata']['address']
        address_field = self._browser.find_by_id(
            self.config['pagedata']['address_id']).first
        address_field.fill(address)

    def fill_mobile_phone(self):
        mobile_phone = self.metadata['pagedata']['mobile_phone']
        mobile_phone_field = self._browser.find_by_id(
            self.config['pagedata']['mobile_phone_id']).first
        mobile_phone_field.fill(mobile_phone)

    def continue_register(self):
        btn = self._browser.find_by_name(
            self.config['pagedata']['button_continue_name']).first
        btn.click()

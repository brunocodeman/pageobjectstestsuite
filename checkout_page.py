from page import Page

class CheckoutPage(Page):

    def __init__(self, browser=None):
        super(CheckoutPage, self).__init__(browser)
        self.visit_url(self.config['pagedata']['url'])

    def proceed_to_checkout(self):
        elem = self.get_element_in_page(self.config['pagedata'][
            'proceed_to_checkout_button'], "name")
        elem.click()

    def select_shipping_method(self):
        self.get_element_in_page(self.config['pagedata'][
            'choose_store_button_class'], "css_selector").click()
        self.wait(5)
        self.remove_element(True, "ClassName", "waitingOverlay")
        self.get_element_in_page(self.config['pagedata'][
            'select_store_button_class'], "class_name").click()

    def accept_terms(self):
        #Using get_element_in_page clicks on the link inside the label and doesn't check the input
        label = self.config['pagedata']['accept_terms_label_xpath']
        jscript = 'document.evaluate("{}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();'
        self.execute_script(jscript.format(label))

    def proceed_to_payment(self):
        elem = self.get_elements_in_page(self.config['pagedata'][
            'complete_my_order_button_name'], "name")[0]
        elem.click()

    def do_checkout(self):
        self.proceed_to_checkout()
        self.wait(5)
        self.select_shipping_method()
        self.wait(5)
        self.accept_terms()
        self.proceed_to_payment()

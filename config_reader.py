"""
This module contains code to import data from your configuration files
"""

import json
CONFIG_FILE_PATH = "./config_files/config.json"
METADATA_FILE_PATH = "./config_files/metadata.json"


class ConfigReader:

    """
    A class to read configuration and metadata to the tests
    """
    @staticmethod
    def get_configuration_for_page(page_name):
        """
        Read URL, field names and other configurations for the specified page_name

        @parameter page_name: The name of the page you will navigate through

        @ptype page_name: string

        @return: An object with the data
        @rtype: dict
        """
        config_file = open(CONFIG_FILE_PATH, 'r')
        pages = json.loads(config_file.read())['pages']
        return find_data(pages, page_name)

    @staticmethod
    def get_metadata_for_page(page_name):
        """
        Read the values to interact with the elements on the specified page

        @parameter page_name: The name of the page you will navigate through
        @ptype page_name: string
        @return: An object with the data
        @rtype: dict
        """
        metadata_file = open(METADATA_FILE_PATH, 'r')
        pages = json.loads(metadata_file.read())['data']
        return find_data(pages, page_name)

    @staticmethod
    def get_config(field_name):
        """
        Returns an specific field from the configuration file

        @param field_name: The field name on your configuration file
        @ptype string

        @return: the value for the specific field name
        @rtype: Object
        """
        config_file = open(CONFIG_FILE_PATH, 'r')
        return json.loads(config_file.read())[field_name]

    @staticmethod
    def get_metadata(field_name):
        """
        Returns an specific field from the metadata file

        @param field_name: The field name on your configuration file
        @ptype string

        @return: the value for the specific field name
        @rtype: Object
        """
        metadata_file = open(METADATA_FILE_PATH, 'r')
        return json.loads(metadata_file.read())[field_name]


def find_data(collection, data_name):
    """
    Searches for a specific data inside a collection of pages

    @param collection: The collection to be searched (Config or Metadata are allowed)
    @ptype: list

    @param data_name: The name of the page you want to retrieve data from Config or Metadata file
    @ptype: string
    """
    obj = [elem for elem in collection if elem['pagename'] == data_name]
    return obj[0]

if __name__ == '__main__':
    print(__doc__)

import time
from selenium.webdriver import Chrome
from login_page import LoginPage
from buy_page import BuyPage
from checkout_page import CheckoutPage
from payment_page import PaymentPage
if __name__ == '__main__':
    loginpage = LoginPage()
    loginpage.do_login()
    b = loginpage._browser
    buypage = BuyPage(b)
    buypage.buy_product()
    checkoutpage = CheckoutPage(b)
    checkoutpage.do_checkout()
    pp = PaymentPage(b)
    pp.pay_purchase()
    print("everything is fine!")
    time.sleep(8)



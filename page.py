"""
Doctring for this module
"""
import time
from selenium.webdriver import Chrome, ChromeOptions
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import WebDriverException
from config_reader import ConfigReader


class Page(object):

    """Page class is the base class for every functional test in this library.
    Whenever you want to create a new test for a new page, your test class must
    inherit from Page class and keep "page" on the name (i.e. LoginPage)."""

    def __init__(self, browser=None):
        """
        Instantiates a Page object and read metadata and fields files

        @param browser: An instance of a WebDriver browser from selenium.webdriver

        """
        self.name = self.name = self.__class__.__name__.split('Page')[
            0].lower()
        options = ChromeOptions()
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--incognito')
        options.add_argument('--start-maximized')
        try:
            self.__load_config()
            self.__load_metadata()
            self._browser = browser or Chrome(chrome_options=options)
            #self._browser.maximize_window()
        except WebDriverException:
            print("Driver not found. Please install Chromedriver.")
        except FileNotFoundError as fnf:
            print("config files missing")
            print(fnf)
            exit()
        

    def enable_element(self, selector='id', attribute_value=''):
        """
        Enable elements with the "disabled" tag.
        @param selector: the type of the selector you want to use.
        The accepted formats are id (default), class_name,css_selector,link_text,
        name, partial_link_text, tag_name and xpath
        @type selector: string
        @param attribute_value: the value of the attribute to be searched
        @type attribute_value: string
        """
        field = self.get_element_in_page(attribute_value, selector)
        self._browser.execute_script(
            'arguments[0].removeAttribute("disabled");', field)

    def wait_until_element_is_present(self, element_identifier, by, timeout=10):
        elem = None
        while elem is None:
            elem = None or self.get_element_in_page(element_identifier, by)
        else:
            return True

    def execute_script(self, script):
        """
        Execute javascript code on the page. This can also be used to trigger events.

        @param script: the script to be executed
        """
        self._browser.execute_script(script)

    def remove_element(self, many=False, select_by="Id", element_identifier=""):
        if many:
            jscript = "Array.from(document.getElementsBy{}('{}')){}"
            foreach = ".forEach(function(item){item.remove();});"
            script_to_run = jscript.format(
                select_by, element_identifier, foreach)
        else:
            jscript = "document.getElementBy{}('{}').remove();"
            script_to_run = jscript.format(select_by, element_identifier)

        self.execute_script(script_to_run)

    def visit_url(self, url_to_visit):
        """
        Visit the url specified by the parameter

        @param url_to_visit: The url to be visited
        @type url_to_visit: string
        @rtype: Page
        @return: The object itself
        """
        self._browser.get(url_to_visit)

    def select_option_from_dropdown(self, element, attribute_value, selector="visible_text"):
        """
        Select a value from a specific <select> field

        @param element: an instance of WebElement to be manipulated
        @type element:  selenium.webdriver.remote.webelement.WebElement

        @param attribute_value: The value of the <option> to be selected
        @type attribute_value: string

        @param selector: The method that will be used to select the <option>. Available values
        are: visible_text(default), index and value
        @type selector: string"""

        dropdown = Select(element)
        method = getattr(dropdown, "select_by{}".format(selector))
        method(attribute_value)

    def get_element_in_page(self, attribute_value, selector="id"):
        """
        Returns an element object

        @param attribute_value: the value of the attribute to be searched
        @type attribute_value: string
        @param selector: the type of the selector you want to use.
        The accepted formats are id (default), class_name,css_selector,link_text,
        name, partial_link_text, tag_name and xpath
        @type selector: string
        @rtype: Element
        @return: The element from the page
        """
        finder = getattr(self._browser, 'find_element_by_{}'.format(selector))
        return finder(attribute_value)

    def get_elements_in_page(self, attribute_value, selector="id"):
        """
        Returns a list of elements

        @param attribute_value: the value of the attribute to be searched
        @type attribute_value: string
        @param selector: the type of the selector you want to use.
        The accepted formats are id (default), class_name,css_selector,link_text,
        name, partial_link_text, tag_name and xpath
        @type selector: string
        @rtype: List<Element>
        @return: A list with all the elements matching the search criteria
        """
        finder = getattr(self._browser, 'find_elements_by_{}'.format(selector))
        return finder(attribute_value)

    def wait(self, seconds):
        """
        Sleep for N seconds

        @param seconds: Time to wait (in seconds).
        @type  seconds: number
        @rtype: Page
        @return: The object itself
        """
        time.sleep(seconds)

    def __del__(self):
        self._browser.close()

    def __load_config(self):
        """
        Loads the configuration file with the elements attributes
        """
        if self.name:
            self.config = ConfigReader.get_configuration_for_page(self.name)

    def __load_metadata(self):
        """
        Loads the metadata file with the elements values
        """
        if self.name:
            self.metadata = ConfigReader.get_metadata_for_page(self.name)

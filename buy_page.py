from page import Page


class BuyPage(Page):

    def __init__(self, browser=None):
        super(BuyPage, self).__init__(browser)
        self.visit_url(self.config['pagedata']['url'])
        self.wait(5)

    def select_size(self):
        size = self.config['pagedata']['size_xpath'].format(
            self.metadata['pagedata']['size'])
        self.get_element_in_page(size, "xpath").click()

    def select_quantity(self):
        amount = self.config['pagedata']['amount_class']
        elems = self.get_elements_in_page(amount, "class_name")
        elems[4].click()

    def click_buy(self):
        self.get_element_in_page(self.config['pagedata'][
                                 'button_id'], "id").click()

    def buy_product(self):
        self.select_size()
        self.remove_element(many=True, select_by="ClassName", element_identifier=self.config[
                            'pagedata']['loader_div_class'])
        self.wait(4)
        self.select_quantity()
        self.click_buy()


def main():
    bp = BuyPage()
    bp.buy_product()

if __name__ == '__main__':
    main()

from page import Page


class PaymentPage(Page):

    def __init__(self, browser=None):
        super(PaymentPage, self).__init__(browser)
        #self.visit_url(self.config['pagedata']['url'])

    def proceed_to_payment(self):
        #shipment_selection = self.get_element_in_page(self.config['pagedata']['shipment_method_radio_id'])
        #shipment_selection.click()
        label = self.config['pagedata']['accept_terms_label_xpath']
        jscript = 'document.evaluate("{}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();'
        self.execute_script(jscript.format(label))
        btn = self.get_element_in_page(self.config['pagedata']['proceed_to_payment_button_name'], "name")
        btn.click()

    def select_card(self):
        radio_btn = self.get_elements_in_page(
            self.config['pagedata']['credit_card_radio_name'], "name")
        radio_btn[1].click()

    def fill_card_number(self):
        cardnumber_input = self.get_element_in_page(
            self.config['pagedata']['credit_card_input_id'])
        cardnumber_input.send_keys(self.metadata['pagedata'][
                                   'credit_card_number'])

    def fill_cardholder_name(self):
        cardnholder_input = self.get_element_in_page(
            self.config['pagedata']['card_holder_input_id'])
        cardnholder_input.send_keys(
            self.metadata['pagedata']['cardholder_name'])

    def select_expiration_month(self):
        elem = self.get_element_in_page(
            self.config['pagedata']['expiration_month_select_id'])
        self.select_option_from_dropdown(
            elem, self.metadata['pagedata']['expiration_month'])

    def select_expiration_year(self):
        elem = self.get_element_in_page(
            self.config['pagedata']['expiration_year_select_id'])
        self.select_option_from_dropdown(
            elem, self.metadata['pagedata']['expiration_year'])

    def fill_security_code(self):
        cvv_input = self.get_element_in_page(
            self.config['pagedata']['security_code_input_id'])
        cvv_input.send_keys(self.metadata['pagedata']['security_code_number'])

    def click_payment_button(self):
        pay_btn = self.get_element_in_page(self.config['pagedata']['payment_button_name'], "name")
        pay_btn.click()

    def  pay_purchase(self):
        #self.proceed_to_payment()
        self.select_card()
        self.fill_card_number()
        self.fill_cardholder_name()
        self.select_expiration_month()
        self.select_expiration_year()
        self.fill_security_code()
        self.click_payment_button()